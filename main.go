/*
File name: main.go
Author: Willem Visscher
Created: 18-jan-2023
Last modified: 13-mar-2023

Description:
This is a simple Go script that scrapes the official police press release website of Munich and message a link to the latest press release into a Telegram channel.

A little script that hustles and bustles,
Scraping press releases, flexing its muscles.
Munich police news, it gathers and channels,
Sending fresh updates to Telegram panels.

*/

package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
	"context"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// =============== settings ===============

// Set to true for verbose debug output to cli
const IsDebug bool = false

// Telegram API URL format
const telegramAPIURL string = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s"

// feedURL to obtain the newsfeed from
const feedURL string= "https://www.polizei.bayern.de/rss_aktuelle_mitteilungen.xml"

// category that we are interested in
const targetCategory string = "Polizeipräsidium München"

// name of the subfolder where the state-files are located
const statefilesDir string = "state_files"

// S3 bucketname where the state files are stored
const s3BucketName string = "kamaradski-pressemitteilungen"

// AWS default region
const awsDefaultRegion string = "eu-central-1"

// how long we sleep between sending the next telegram message
const telegramSleepDuration time.Duration = 1 * time.Second

// =============== /settings ===============


// StructNewsItem represents a single news item from the RSS feed
type StructNewsItem struct {
	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Description string `xml:"description"`
	Category    string `xml:"category"`
	PubDate     string `xml:"pubDate"`
}

type Rss struct {
	XMLName xml.Name `xml:"rss"`
	Channel struct {
			Title       string `xml:"title"`
			Link        string `xml:"link"`
			Description string `xml:"description"`
			Items       []StructNewsItem `xml:"item"`
	} `xml:"channel"`
}

// funcCheckstatefilesDir checks if the statefiles directory exists in the S3 bucket and creates it if it doesn't
//
// Args:
// None
//
// Returns:
// None
//
// Example usage:
// funcCheckstatefilesDir()
func funcCheckstatefilesDir() {
	log.Print("function: funcCheckstatefilesDir()\n")

	// Create an AWS session
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(awsDefaultRegion),
	})
	if err != nil {
		log.Fatalf("Error creating new AWS session: %v", err)
	}

	// Create an S3 client
	svc := s3.New(sess)

	// Check if the "statefiles" directory exists in the S3 bucket
	_, err = svc.HeadObject(&s3.HeadObjectInput{
		Bucket: aws.String(s3BucketName),
		Key:    aws.String(statefilesDir + "/"),
	})
	if err == nil {
		// The directory already exists in the S3 bucket
		return
	}

	// Create the "statefiles" directory in the S3 bucket
	_, err = svc.PutObject(&s3.PutObjectInput{
		Bucket: aws.String(s3BucketName),
		Key:    aws.String(statefilesDir + "/"),
		Body:   bytes.NewReader([]byte("")),
	})
	if err != nil {
		log.Fatalf("Error creating statefilesDir directory in S3 bucket: %v", err)
	}
}

// funcReadRSSFeed reads the RSS feed and returns a slice of StructNewsItem pointers.
//
// Args:
// - feedURL: string representing the URL of the RSS feed to read.
// example data: "https://www.polizei.bayern.de/rss_aktuelle_mitteilungen.xml"
//
// Returns:
// - []*StructNewsItem: a slice of pointers to StructNewsItem objects, representing the news items read from the RSS feed.
// example data:
// [
// 	&StructNewsItem{
// 		Title: "Pressebericht der Polizei München vom 12.03.2023",
// 		Link: "https://polizei.bayern.de/aktuelles/pressemitteilungen/045483/index.html",
// 		Description: "Pressebericht der Polizei München vom 12.03.2023",
// 		Category: "Polizeipräsidium München",
// 		PubDate: "Sun, 12 Mar 2023 14:28:00",
// 	},
// 	&StructNewsItem{
// 		Title: "Umhängetasche aus unverschlossenem Renault gestohlen",
// 		Link: "https://polizei.bayern.de/aktuelles/pressemitteilungen/045487/index.html",
// 		Description: "Umhängetasche aus unverschlossenem Renault gestohlen",
// 		Category: "Polizeipräsidium Unterfranken",
// 		PubDate: "Sun, 12 Mar 2023 17:41:00",
// 	},
// ]
//
// Example usage:
// 	newsItems := funcReadRSSFeed("https://www.polizei.bayern.de/rss_aktuelle_mitteilungen.xml")
func funcReadRSSFeed(feedURL string) []*StructNewsItem {
	log.Printf("function: funcReadRSSFeed()\n")
	resp, err := http.Get(feedURL)
	if err != nil {
			log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
			log.Fatal(err)
	}

	rss := &Rss{}
	err = xml.Unmarshal(body, rss)
	if err != nil {
			log.Fatal(err)
	}

	// Convert slice of StructNewsItem to slice of pointers to StructNewsItem
	items := make([]*StructNewsItem, len(rss.Channel.Items))
	for i, v := range rss.Channel.Items {
			item := &StructNewsItem{
					Title:       v.Title,
					Link:        v.Link,
					Description: v.Description,
					Category:    v.Category,
					PubDate:     v.PubDate,
			}
			items[i] = item
	}

	return items
}



// removeItemsByCategory modifies the original slice by removing news items that do not match the target category.
//
// Args:
// - storedNewsItems: pointer to a slice of pointers to StructNewsItem; the slice to be filtered.
//      Example data:
//      []*StructNewsItem{
//          &StructNewsItem{
//              Title: "Pressebericht der Polizei München vom 12.03.2023",
//              Link: "https://polizei.bayern.de/aktuelles/pressemitteilungen/045483/index.html",
//              Description: "Pressebericht der Polizei München vom 12.03.2023",
//              Category: "Polizeipräsidium München",
//              PubDate: "Sun, 12 Mar 2023 14:28:00",
//          },
//          &StructNewsItem{
//              Title: "Umhängetasche aus unverschlossenem Renault gestohlen ",
//              Link: "https://polizei.bayern.de/aktuelles/pressemitteilungen/045487/index.html",
//              Description: "Umhängetasche aus unverschlossenem Renault gestohlen ",
//              Category: "Polizeipräsidium Unterfranken",
//              PubDate: "Sun, 12 Mar 2023 17:41:00",
//          },
//          ...
//      }
// - targetCategory: string; the category to filter for.
// example data: "Polizeipräsidium München"
//
// Returns: none. (manipulates the original pointer to a slice of pointers to StructNewsItem)
//
// Example usage:
// removeItemsByCategory(&storedNewsItems, "Polizeipräsidium München")
func removeItemsByCategory(storedNewsItems *[]*StructNewsItem, targetCategory string) {
	log.Printf("function: removeItemsByCategory()\n")

	// iterate over the slice of news items and remove items that do not match the target category
	for i := 0; i < len(*storedNewsItems); {
		if (*storedNewsItems)[i].Category != targetCategory {
			// remove the item from the slice
			*storedNewsItems = append((*storedNewsItems)[:i], (*storedNewsItems)[i+1:]...)
		} else {
			// increment the index if the item matches the target category
			i++
		}
	}
}



// funcProcessNewsItems saves new news to file and removes items that have been seen before from the slice of news items.
//
// Args:
// - newsItems: a pointer to a slice of pointers to StructNewsItem, representing the new news items to be processed.
//      Example data:
//      []*StructNewsItem{
//          &StructNewsItem{
//              Title: "Pressebericht der Polizei München vom 12.03.2023",
//              Link: "https://polizei.bayern.de/aktuelles/pressemitteilungen/045483/index.html",
//              Description: "Pressebericht der Polizei München vom 12.03.2023",
//              Category: "Polizeipräsidium München",
//              PubDate: "Sun, 12 Mar 2023 14:28:00",
//          },
//          ...
//      }
//
// Returns:
// - none (manipulates the original pointer to a slice of pointers to StructNewsItem)
//
// Example usage:
// funcProcessNewsItems(&newsItems)
func funcProcessNewsItems(newsItems *[]*StructNewsItem) {
	log.Printf("function: funcProcessNewsItems()\n")
	var sliceStringNewNews []string
	for i := len(*newsItems) - 1; i >= 0; i-- {
			item := (*newsItems)[i]

			// Generate the filename
			filename := strings.ReplaceAll(item.Title, " ", "_")
			filename = url.PathEscape(filename)
			filename = strings.ReplaceAll(filename, "%", "")
			filename = filename + ".json"

			if exists := isFileExists(filename); exists {
				*newsItems = append((*newsItems)[:i], (*newsItems)[i+1:]...)
				continue
			}

			// Save the news item to file
			saveNewsItem(*item, filename)
			sliceStringNewNews = append(sliceStringNewNews, item.Title)
	}
}


// isFileExists checks if a file exists in the S3 bucket.
//
// Args:
// - filename: string representing the filename to check.
//      example data: "example.json"
//
// Returns:
// - bool indicating whether the file exists or not
//      example data: true
//
// - error indicating whether an error occurred while checking for file existence in S3
//      example data: error checking if file exists on S3: NoSuchKey: The specified key does not exist.
//
// Example usage:
// - Check if file "example.json" exists
//     result, err := isFileExists("example.json")
//     if err != nil {
//         log.Fatal(err)
//     }
//     if result {
//         log.Print("File exists in S3 bucket")
//     } else {
//         log.Print("File does not exist in S3 bucket")
//     }
func isFileExists(filename string) bool {
	log.Printf("function: isFileExists()\n")

	// Create an AWS session
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(awsDefaultRegion),
	})
	if err != nil {
		log.Printf("error creating new AWS session: %v", err)
		return false
	}
	if IsDebug {
		log.Printf("we have created a session\n")
	}

	// Create an S3 client
	svc := s3.New(sess)
	if IsDebug {
		log.Printf("we have created a client\n")
	}

	headObjectInput := &s3.HeadObjectInput{
		Bucket: aws.String(s3BucketName),
		Key:    aws.String(statefilesDir + "/" + filename),
	}
	_, err = svc.HeadObject(headObjectInput)
	if err == nil {
		// The file already exists in the S3 bucket
		return true
	}

	// Check if it's a "NoSuchKey" error
	if aerr, ok := err.(awserr.Error); ok && aerr.Code() == "NoSuchKey" {
		// The file doesn't exist in the S3 bucket
		return false
	}

	log.Printf("error checking if file exists on S3: %v", err)
	return false
}


// saveNewsItem saves a NewsItem to an S3 bucket
//
// Args:
// - newsItem: *StructNewsItem - pointer to the news item that needs to be saved to file.
// Description: A pointer to the news item that will be saved to file.
// Example: &StructNewsItem{Title: "Pressebericht der Polizei München vom 12.03.2023", Link: "https://polizei.bayern.de/aktuelles/pressemitteilungen/045483/index.html", Description: "Pressebericht der Polizei München vom 12.03.2023", Category: "Polizeipräsidium München", PubDate: "Sun, 12 Mar 2023 14:28:00"}
// - filename: string - the filename (including path) of the file to which the news item will be saved.
// Description: The name of the file (including the path) where the news item will be saved.
// Example: "statefilesDir/Pressebericht_der_Polizei_MC3BCchen_vom_12.03.2023.json"
//
// Returns:
// - error - if an error occurs while saving the news item to file, it is returned.
// Description: Any error that occurred while saving the news item to file.
//
// Example usage:
// err := saveNewsItem(*item, "statefilesDir/Pressebericht_der_Polizei_MC3BCchen_vom_12.03.2023.json")
func saveNewsItem(newsItem StructNewsItem, filename string) error {
	log.Printf("function: saveNewsItem()\n")

	// Create an AWS session
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(awsDefaultRegion),
	})
	if err != nil {
		return fmt.Errorf("error creating new AWS session: %v", err)
	}

	// Create an S3 client
	svc := s3.New(sess)

	// Create the S3 bucket key based on the filename
	key := statefilesDir + "/" + filename

	// Convert the news item to JSON
	newsItemJSON, err := json.Marshal(newsItem)
	if err != nil {
		return fmt.Errorf("error converting news item to JSON: %v", err)
	}

	// Upload the JSON data to the S3 bucket
	_, err = svc.PutObject(&s3.PutObjectInput{
		Bucket: aws.String(s3BucketName),
		Key:    aws.String(key),
		Body:   bytes.NewReader(newsItemJSON),
	})
	if err != nil {
		return fmt.Errorf("error uploading news item to S3 bucket: %v", err)
	}

	return nil
}



// sendTelegramMessage sends a Telegram message for each new news item.
//
// Args:
// - item: a pointer to a StructNewsItem that contains the news item to be sent as a message.
//         The struct should have the following fields: Title (string), Description (string),
//         Link (string), Category (string), PubDate (string).
//         Example data:
//           &StructNewsItem{
//                Title:       "Pressebericht der Polizei München vom 12.03.2023",
//                Link:        "https://polizei.bayern.de/aktuelles/pressemitteilungen/045483/index.html",
//                Description: "Pressebericht der Polizei München vom 12.03.2023",
//                Category:    "Polizeipräsidium München",
//                PubDate:     "Sun, 12 Mar 2023 14:28:00",
//           }
//
// Returns: nothing
//
// Example usage:
//   item := &StructNewsItem{
//        Title:       "Pressebericht der Polizei München vom 12.03.2023",
//        Link:        "https://polizei.bayern.de/aktuelles/pressemitteilungen/045483/index.html",
//        Description: "Pressebericht der Polizei München vom 12.03.2023",
//        Category:    "Polizeipräsidium München",
//        PubDate:     "Sun, 12 Mar 2023 14:28:00",
//   }
//   sendTelegramMessage(item)
func sendTelegramMessage(item *StructNewsItem) {
	log.Print("function: sendTelegramMessage()\n")

	// Format the message
	//msg := fmt.Sprintf("%s\n%s\n%s", item.Title, item.Description, item.Link)
	msg := fmt.Sprintf("%s\n%s", item.Title, item.Link)

	// Send the message via Telegram API
	apiURL := fmt.Sprintf(telegramAPIURL, os.Getenv("TELEGRAM_API_KEY"), os.Getenv("CHAT_ID"), url.QueryEscape(msg))
	log.Printf("telegram apiURL: %v\n", apiURL)
	resp, err := http.Get(apiURL)
	if err != nil {
			log.Printf("Error sending Telegram message: %v\n", err)
			return
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
			log.Printf("Error reading Telegram response: %v\n", err)
			return
	}

	log.Printf("Telegram message sent: %s\n", string(body))
}

func HandleRequest(ctx context.Context, event json.RawMessage) (interface{}, error) {
	log.Print("function: main()\n")

	// Check if the statefilesDir directory exists
	funcCheckstatefilesDir()

	// Read the RSS feed
	storedNewsItems := funcReadRSSFeed(feedURL)
	if len(storedNewsItems) == 0 {
    log.Fatal("No news items found, is the feed empty?")
	}
	if IsDebug {
		log.Print("\n\n==================================== [ALL NEWS]\n")
		for _, item := range storedNewsItems {
			log.Printf("%+v\n\n", item)
		}
	}

	// Filter the news items by category
	removeItemsByCategory(&storedNewsItems, targetCategory)
	if len(storedNewsItems) == 0 {
    log.Fatal("No news items found for the target category")
	}
	if IsDebug {
		log.Print("\n\n==================================== [FILTERED]\n")
		for _, item := range storedNewsItems {
			log.Printf("%+v\n\n", item)
		}
	}

	// save new news to file, if news has been seen before we remove the item from the slice
	funcProcessNewsItems(&storedNewsItems)
	if len(storedNewsItems) == 0 {
    log.Fatal("All news is old, exiting")
	}
	if IsDebug {
		log.Print("\n\n==================================== [ONLY NEW NEWS]\n")
		for _, item := range storedNewsItems {
			log.Printf("%+v\n\n", item)
		}
	}

	// send a telegram message for all new news
	for _, item := range storedNewsItems {
		sendTelegramMessage(item)
		time.Sleep(telegramSleepDuration)
	}

	log.Print("Script finished")
	return "success", nil
}

func main() {
	lambda.Start(HandleRequest)
}