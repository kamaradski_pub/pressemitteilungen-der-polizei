#!/bin/bash

# A bash script to list and display,
# The files of the bucket all day,
# Modified times, dates, and order,
# With a humorous border,
# Our rhyme celebrates this code play.

# Set bucket and folder name
BUCKET="kamaradski-pressemitteilungen"
FOLDER="state_files"

# Get the list of files in the folder with last modified date and time, and sort them
FILES=$(aws s3 ls s3://$BUCKET/$FOLDER/ | sort | awk '{print $1 " " $2 " " $4}')

# Display the list of files as a numbered option-menu
echo "Select a file to display:"
options=()
while read -r date time file; do
  options+=("$file")
  echo "$((${#options[@]})). $file - Last modified: $date $time"
done <<< "$FILES"

# Read user input
read -p "Enter the number of the file you want to display: " file_number

# Check if the input is valid
if [[ $file_number -gt 0 ]] && [[ $file_number -le ${#options[@]} ]]; then
  # Display the content of the file
  aws s3 cp s3://$BUCKET/$FOLDER/${options[$(($file_number - 1))]} - | cat
else
  echo "Invalid input. Please enter a number between 1 and ${#options[@]}."
fi
