# Welcome to the Police Press Release Scraper

This project is a simple Go script that scrapes the official police press release website of Munich and sends the latest press release to a Telegram channel. The script uses the Google Custom Search API to search for the latest press release, and the Telegram Bot API to send the press release to a Telegram channel.  

It is the driving code for the following telegram channel: [Pressemitteilungen der Polizei München](https://t.me/PressemitteilungenPolizeiMunchen)

## Twitter pitch

"Neu und exklusiv: Verpassen Sie keine wichtigen Meldungen der Polizei München mehr! Folgen Sie jetzt unserem Telegram-Kanal: https://t.me/PressePolizeiMuenchen #Polizei #München #Pressemitteilungen"

## Getting started

- To get started, you will need to create a Telegram bot and obtain the bot's API token. You can do this by talking to the [@BotFather](https://t.me/BotFather) on Telegram.  

- You will also need to create a Google Custom Search Engine and obtain the search engine ID and API key. You can go this at the [programmablesearchengine portal](https://programmablesearchengine.google.com/) from Google.  

- The script requires you to set the following environment variables:  

  - `TELEGRAM_API_KEY`: the Telegram bot API token obtained from the BotFather
  - `CHAT_ID`: The chatID of the chat you are using to communicate with the bot (usually a group)
  - `GOOGLE_SEARCH_CX`: the search engine ID obtained from Google Custom Search
  - `GOOGLE_API_KEY`: the API key obtained from Google Custom Search
  - `SEARCHSTRING`: The title of the main article to search for

- And you should install the following Go libraries on which the script depends on:  

  - `github.com/PuerkitoBio/goquery`: used to scrape the HTML content of the police press release website
  - `github.com/go-telegram-bot-api/telegram-bot-api`: used to interact with the Telegram bot API
  - `github.com/aws/aws-lambda-go/lambda`: AWS lambda library required for the Lambda handler function, see: [here](https://docs.aws.amazon.com/lambda/latest/dg/golang-handler.html)

- You can install these dependencies by running the following commands:  

    ```sh
    go get -u github.com/PuerkitoBio/goquery 
    go get github.com/go-telegram-bot-api/telegram-bot-api
    go get github.com/aws/aws-lambda-go/lambda
    ```

- Once you have obtained the necessary API keys and set the environment variables, you can build and run the script with:  

  ```sh
  go build -o main main.go
  ./main
  ```

## Telegram

- You will need the chatID of your direct chat with the bot, the group you made it part of, or the channel this bot is in.

  - You can do this with curl as such:

    ```sh
    curl https://api.telegram.org/bot<YOUR_TELEGRAM_API_KEY>/getUpdates
    ```

  - Or you can add the [@MissRose_bot](https://t.me/MissRose_bot) to the group/channel

  - Or any of the available other methods on the public internet

## Usage Note

This program is scraping a website, it's important to be aware of the website's terms of service and if it's allowed to scrape. If an article's content is longer than 4096 characters, it will be truncated when sent in a Telegram message.

## Limitations

- The script currently only works for Pressebericht der Polizei München
- It only sends the press release of the previous day
- It only sends the press release to a single Telegram chat

## Contributions

- Pull requests are welcome
- If you have any suggestions or issues, please open them

## License

This code is open-sourced under the [MIT license](https://opensource.org/licenses/MIT). Please feel free to use, modify, and distribute the code as you wish.
