module gopkg.in/telegram-bot-api.v4

go 1.19

//require github.com/PuerkitoBio/goquery v1.8.0

//require github.com/aws/aws-lambda-go v1.37.0 // indirect

require github.com/aws/aws-sdk-go v1.44.219

require (
	github.com/aws/aws-lambda-go v1.38.0
	github.com/jmespath/go-jmespath v0.4.0 // indirect
)
